# PSL/Sugar Syntax Support

This extension provides simple highlighting support for PSL/Sugar language used for formal testing.

## Features
- Highlighting of keywords, operators and built-in functions for files with `.psl` extension.

![](./img/code-example.png)


## Installation
Search for `PSL/Sugar Syntax` in VS Code marketplace and install it.

